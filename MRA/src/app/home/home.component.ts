import { Component, OnInit } from '@angular/core';
import "../../assets/js/wallet.js"
declare var walletConnect: any;
declare var getAccount: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  walletConnected = false;
  account: any
  constructor() { }

  ngOnInit(): void {
    setTimeout(() =>{
      new walletConnect()
    }, 3000)
  }

  getAccount() {
    //@ts-ignore
    window.ethereum.request({ method: 'eth_requestAccounts' })
      .then((res: any) => {
        if(!!res){
          if(res.length > 0){
            this.account =  res[0];
            //@ts-ignore
            window.ethereum.request({
              method: 'wallet_switchEthereumChain',
              params: [{ chainId: '0x89' }], // chainId must be in hexadecimal numbers
            }).then(()=>{
              this.walletConnected = true
            }).catch((error: any) => {
              this.walletConnected = false
              alert('you should add MATIC network to your metamask account')
            });
          }
        }
      }).catch((error: any) => {
      this.walletConnected = false
      this.account = null
    });
  }
    /*//@ts-ignore
    window.ethereum.request({
      method: 'wallet_switchEthereumChain',
      params: [{ chainId: '0x89' }], // chainId must be in hexadecimal numbers
    }).then(() => {
        //@ts-ignore
        window.ethereum.request({ method: 'eth_requestAccounts' })
          .then((res: any) => {
            if(!!res){
              if(res.length > 0){
                this.walletConnected = true
                this.account =  res[0]
              }
            }
          }).catch((error: any) => {
          this.walletConnected = false
          this.account = null
        });
      }
    ).catch((error: any) => {
      alert('you should add MATIC network to your metamask account')
    });
     */


  mintEtherieum(){
    //@ts-ignore
    window.ethereum
      .request({
        method: 'eth_sendTransaction',
        params: [
          {
            from: this.account,
            to: '0xc0f3840e97700e2f19ef48e3e98a5ff309f57cba',
            value: '0xAD78EBC5AC6200000',
            data: "0x755edd17000000000000000000000000"+(this.account.substring(2))
          },
        ],
      })
      .then((txHash: any) => {
        alert("NFT Badge Minted \n Transaction Hash : "+txHash)
      } )
      .catch((error: any) => alert("Error NFT Badge not Minted : "+error.message));
  }
}
